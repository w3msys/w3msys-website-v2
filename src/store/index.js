import Vue from 'vue';
import Vuex from 'vuex';
import state from './state';

Vue.use(Vuex);

const store = new Vuex.Store({
    state,
    getters: {
        getProducts (state) {
            return state.products
        },
        getSelectedProduct (state) {
            return state.selectedProduct
        },
        getServices (state) {
            return state.services
        },
        getTeam (state) {
            return state.team
        },
        getSelectedTeam (state) {
            return state.selectedTeam
        },
        getSelectedService (state) {
            return state.selectedService
        }
    },
    actions: {
        setSelectedProduct ({commit}, payload) {
            commit('SET_SELECTED_PRODUCT', payload)
        },
        setSelectedTeam ({commit}, payload) {
            commit('SET_SELECTED_TEAM', payload)
        },
        setSelectedService({commit}, payload) {
            commit('SET_SELECTED_SERVICE', payload)
        }
    },
    mutations: {
        SET_SELECTED_PRODUCT (state, payload) {
            for (let i = 0; i < state.products.length; i++) {
                if (state.products[i].id == payload) {
                    state.selectedProduct = state.products[i]
                    return
                }
            }
        },
        SET_SELECTED_TEAM (state, payload) {
            for (let i = 0; i < state.team.length; i++) {
                if (state.team[i].id == payload) {
                    state.selectedTeam = state.team[i]
                    return
                }
            }
        },
        SET_SELECTED_SERVICE (state, payload) {
            for (let i = 0; i < state.services.length; i++) {
                if (state.services[i].id == payload) {
                    state.selectedService = state.services[i]
                    return
                }
            }
        }
    }
})

export default store;