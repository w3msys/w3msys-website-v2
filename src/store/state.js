export default {
    selectedProduct: {name: 'product'},
    selectedTeam: {},
    selectedService: {},
    team: [
        {
            id: 1,
            name: 'Kwabena Owusu Danquah',
            role: '(Founder & CEO)',
            content: `Kwabena Danquah is a computing futurist with special interest in functional application of
            biology and psychology in computing. He was the former Head of Sonography
            department and also served as acting Head of Department of Medical Laboratory on
            several occasions, of the Faculty of Allied Health Sciences (FAHS) of Kwame Nkrumah
            University of Science & Technology (KNUST), Kumasi-Ghana.
            He worked as a Senior Scientific Officer at a leading Biotech company-Oxford Expression
            Technologies in Oxford, UK. He completed two post-doctoral fellowships at Oxford
            Brookes University, Oxford-UK and University College of Dublin, in Dublin of Republic
            of Ireland.
            Kwabena Owusu holds a doctorate degree (PhD) in Cellular & Molecular Virology and
            an MSc Biomedical Sciences, both from UK. He has completed several online courses in
            programming, cognitive psychology, resource and human management and modeling.`,
            image: 'danquah.jpg',
            email: 'kodanquah@w3msys.com'
        },
        {
            id: 2,
            name: 'William Morgan-Darko',
            role: '(Co-Founder & CTO)',
            content: `William is an IT researcher and software developer. He has undertaken many projects,
            working in a variety of programming languages and using various programming
            technologies. His preferred programming languages are Java, JavaScript and PHP
            although he has worked in Python and C++ as well.
            A selected few list of projects and research he has worked on include Speech Synthesis;
            Data Security in the Cloud; Data Encryption; Object Recognition in Computer Vision and
            Mobile Forensics.
            William holds a BSc Computer Science (First Class Honours) from KNUST and has worked
            as a Teaching and Research Assistant at the Department of Computer Science at KNUST.`,
            image: 'morgan.jpg',
            email: 'wmorgan-darko@w3msys.com'
        },
        {
            id: 3,
            name: 'Joshua Akonor-Ntow',
            role: '(Creative Director & UI Designer)',
            content: `Joshua is the head of creative and User Interface (UI) design department of W3MSYS.
            His major specialties include graphic, motion, and UI designing, advertising and
            photography. He spent more than 6 years working at FOFA Origin, Innovatestudios,
            Ashmetro Prints, and TekTv.
            Joshua was the brain behind “Our Monuments” (MTN App Challenge S4 winner),
            “Eno Chips” (Ghana Graphic Design Awards 2017, winner, Packaging Design category),
            AC Library (mobile app), and others.
            He graduated with BSc in Communication Design (First Class Honors) from the
            Kwame Nkrumah University of Science and Technology (KNUST-Kumasi). He has won
            several awards and certificates during his educational ladder, and in his field of profession.`,
            image: 'josh.jpg',
            email: 'jakonorntow@w3msys.com'
        },
        // {
        //     id: 4,
        //     name: 'Dawuda Ahmed',
        //     role: '(Software Developer)',
        //     content: `Ahmed is part of the software development team, with keen interest in the front-end
        //     software development. He has extensive, rich and varied experiences in software
        //     development and its tools for both web and mobile(android and IOS) systems.
        //     A selected few programming languages that Ahmed is conversant with include PHP,
        //     Laravel, HTML, CSS, Java, JavaScript, Vue.js, React.js, Electron JS, Progressive web app technologies, MySQL,
        //     MongoDB, Web socket, CSS design, Sass.
        //     Ahmed has undertaken and completed some elegant works on a contract basis for different
        //     companies prior to joining W3MSYS. These are Pescedi Virtual Reality, Currency Converter app, CSS Art Living room,
        //     CSS Animated Bicycle, Checksum Data recovery, Online-voting system and Sales-Payment-Inventory & Cash-tracking management system.`,
        //     image: 'ahmed.jpg',
        //     email: 'dahmed@w3msys.com
        // },
        {
            id: 4,
            name: 'Anna Safoa Ofori',
            role: '(Human Resources & Marketing Manager)',
            content: `Anna worked at Xtrem Technology Company as the business development manager.
            Prior to that she was a client acquirer at Universal Merchant Bank. She also worked as
            an Executive assistant at Global Marine Consult in Accra.
            She graduated from University of Ghana, Legon, with BA in History and Theatre Arts.
            Anna also had a foundation course in ICT at Intercom Programming and Manufacturing
            Company (IPMC). She is an associate member of Institute of Public Relations, Accra.`,
            image: 'anna.jpg'
        },
        {
            id: 5,
            name: 'Marian Mensah',
            role: '(Legal Consultant)',
            content: `Marian has been a practising lawyer at the Blay & Associates firm in Accra, 
            Ghana. She was called to the Ghana Bar in 2014. Prior to that she was called to UK Bar,
             Lincoln ‘Inn in 2012. She graduated from University of Ghana, Accra-Ghana with BA Information Studies.`,
            image: 'mariam.jpg'
        }
    ],
    services: [
        {
            name: 'software development',
            content: `At W3MSYS, we use only the latest technologies to craft highly secure, functionally
            efficient and visually appealing software for all purposes and platforms. Every step
            of the development process receives a careful attention, be it the development of
            the system data flow, user experience and user interface design, database management,
            security architecture design and all else. Our team of skillful and enterprising young
            developers and designers are well versed in their craft and develop only the best
            quality web, mobile and desktop applications for our clients.
            Contact us for your websites, Online Marketing applications, School and Church
            Management software, Sales and Inventory management applications, Enterprise
            Resource Planning software, Management Information Systems, Accounting Software,
            Process Control Systems, Inter-office Communications Systems, Product Tracking
            Systems and more.`,
            id: 1,
            image: 'software-dev'
        },
        {
            name: 'brand development',
            content: `We understand the need for an excellent representational expression of yourself,
            your company and your products. That is why we devote our best and brightest to
            the crafting of a globally competitive brand image for your outfit. Every line, shape
            and colour are well thought out and placed to draw maximum attention to your
            products and identity.
            Your brand is you! And there must be no compromise when it comes to the message
            you send across to the world on who you are, what you do and what you stand for.
            At W3MSYS, we understand that, making us very well suited to handle your brand.
            Contact us for your expertly designed logos, contact cards, letterheads, graphic
            designs and video adverts, and rest assured of the best treatment possible.`,
            id: 2,
            image: 'brand-dev'
        },
        {
            name: 'data science',
            content: `Regular business operations generate a lot of data which contains information about
            what products move at what time and which customers prefer which products and
            services. Data science, as conducted at W3MSYS, concerns itself with extracting
            specific business-relevant data from the data accumulated from operations.
            Do you want to know which people are more likely to respond to your products?
            Do you want to know which marketing methods are best for your target consumers?
            Let us extract that information from your data.
            We will assemble, organize, mine and clarify your data to give you maximum utility
            from your records.`,
            id: 3,
            image: 'data-science'
        },
        {
            name: 'biocomputing',
            content: `At the heart of W3MSYS is a driving cutting-edge research, conducted in a collaborative
            manner among the universities in Ghana, to push the boundaries of biological systems
            in computing. Our research is geared towards understanding some biological
            phenomena such as emotions, smell and visual, and their applications in Artificial
            Intelligence (AI). This is a long-term goal of W3MSYS in the field of AI.`,
            id: 4,
            image: 'biocomputing'
        }
    ],
    products: [
        {
            name: 'siesie',
            description: 'a mobile app for getting a mechanic, plumber, carpenter, builder, artisan.',
            id: 1,
            title: 'Don’t Let the Hustle Control You. Control the Hustle.',
            content: `Frustrated by the breakdown of your car in the middle of the road? Need a wooden
            shelter for your dog? Wish those nagging electrical problems in the house are fixed
            permanently, and your bathroom is not frequently flooded because of poorly sealed
            pipes? SIESIE app takes care of that for you by providing well certified, experienced
            and tested plumbers, carpenters, electricians, mechanics and all sort of artisans, who
            have their criminal records clean.`
        },
        {
            name: 'yori',
            description: 'a mobile app for ordering drugs from pharmacy; booking medical, physiotherapy and veterinary appointments.',
            id: 2,
            title: 'Get Healed!',
            content: `YORI app brings healing to your doorpost, but to your pets too.
            Having YORI app is having a real-time stock of all drugs in all pharmacy and chemical
            drug stores in the country in your hand. With it comes the luxury of accessing medical
            doctors far quicker than before, buying both prescribed and non-prescribed drugs
            without stepping a foot at the pharmacy, making veterinary and physiotherapist
            appointments. Use YORI. Get Healed!!`
        },
        {
            name: 'seeli',
            description: 'a mobile app for sending and receiving gift voucher, fuel coupons, restaurant voucher.',
            id: 3,
            title: 'Anniversaries, Birthdays, Surprises!',
            content: `You have been planning for a while on giving special gifts and surprises to your friends,
            family and colleagues on memorable occassions, and for all the love, care and supports
            you have received from them. You can do it NOW with gift coupons - that can buy gifts.
            SEELI app also supports fuel coupons to fill your fuel tanks from any filling station in
            the country.`
        },
        {
            name: 'tika',
            description: 'a mobile app for ordering food and drinks from restaurants, food joints and bars.',
            id: 4,
            title: 'Eat Out!',
            content: `Chill out with friends in the pubs, bars and drinking spots anywhere, anytime.
            TIKA app makes these and more so easy and so satisfying.
            TIKA app brings a pallette of restaurants, bars, chopbars, pubs and drinking spots
            at every corner in the country all together for easy ordering of foods. Get TIKA to
            order pizzas, any African, Continental and savoury dishes you crave for, reserve or
            book a place for dinning, parties and other gatherings. TIKA is all your satisfaction.`
        },
        {
            name: 'sio',
            description: 'a mobile app for searching for a salon, getting a stylist, massaging and buying bodycare products',
            id: 5,
            title: 'Pamper Yourself with Soothing Massage!',
            content: `Looking for the best salons and beauty shops in your neighbourhood to fix your hair
            or do your makeup or cut your hair? What about comparing all the cosmetics shops
            around to buy high quality cosmetics products? SIO app enables you to just do that.`
        },
        {
            name: 'cumbolo',
            description: 'a mobile app for searching for tourism sites and places & events in Ghana.',
            id: 6,
            title: 'Cumbolo, Let’s Have Fun Together.',
            content: `Make your sabbaticals, holidays and day-outs memorable with CUMBOLO app,
            which shows you tourism sites - gardens, castles and forts, caves, waterfalls, forests
            endowed with exotic flora and fauna. CUMBOLO also gives you real time cultural
            festivals and other events in every location in the country. Let CUMBOLO book touristic
            and pristine places for your royal weddings, gathering and many other celebrations.`
        },
        {
            name: 'pelum',
            description: 'A mobile app for searching and booking for a hotel, hostel & guesthouse; and facilities such as conference rooms and swimming pools.',
            id: 7,
            title: 'Pelum is Comfort. Stay Comfortable',
            content: `Now! For the first time you can easily access available rooms of any hotels, guesthouses,
            hostels and sharerooms of your taste and cost anytime. PELUM app helps you to make
            a reservation in the shortest possible time for a hotel room or a hotel facility such as
            restaurants, swimming pool, conference rooms without having to go to the location.`
        }
    ]
}