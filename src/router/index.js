import Vue from 'vue';
import Router from 'vue-router';
import routes from './routes';


Vue.use(Router)

const router = new Router({
  routes, 
  scrollBehavior: (to, from, savedPosition) => {
    return { x: 0, y: 0 }
  }
})

router.afterEach((to, from) => {
  document.getElementsByTagName('title')[0].innerText = to.meta.title
})

export default router