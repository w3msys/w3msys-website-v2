import {WHome, WProducts, WSingleProduct, WServices, WAbout, WSingleTeam, WSingleService} from './../components';

export default [
    {
      path: '/',
      redirect: '/welcome'
    },
    {
      path: '/welcome',
      name: 'home',
      meta: {title: 'Welcome to W3msys'},
      component: WHome
    },
    {
      path: '/products',
      name: 'products',
      meta: {title: 'See Our Products'},
      component: WProducts
    },
    {
      path: '/product/:product_id',
      name: 'singleproduct',
      meta: {title: 'View this Product'},
      component: WSingleProduct,
      props: true
    },
    {
      path: '/services',
      name: 'services',
      meta: {title: 'See Our Services'},
      component: WServices
    },
    {
      path: '/about',
      name: 'about',
      meta: {title: 'See Who we are'},
      component: WAbout
    },
    {
      path: '/team/:team_id',
      name: 'singleteam',
      meta: {title: 'See Our team member'},
      component: WSingleTeam,
      props: true
    },
    {
      name: 'singleservice',
      path: '/service/:service_id',
      meta: {title: 'View this Service'},
      component: WSingleService,
      props: true
    }
]