import header from './commons/header.vue';
import footer from './commons/footer.vue';
import mobileNav from './commons/mobile.vue';

import home from './home.vue';
import products from './products.vue';
import singleProduct from './singleProduct.vue';
import services from './services.vue';
import team from './team.vue';
import about from './about.vue';
import singleTeam from './singleTeam.vue';
import singleService from './singleService.vue';


export const WHeader = header;
export const WFooter = footer;
export const WMobileNav = mobileNav;
export const WHome = home;
export const WProducts = products;
export const WSingleProduct = singleProduct;
export const WServices = services;
export const WTeam = team;
export const WAbout = about;
export const WSingleTeam = singleTeam;
export const WSingleService = singleService;